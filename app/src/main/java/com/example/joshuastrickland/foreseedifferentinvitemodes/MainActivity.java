package com.example.joshuastrickland.foreseedifferentinvitemodes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.foresee.sdk.ForeSee;

/* Captain's Log

android:name - The fully qualified name of an Application subclass implemented for the application. When the application process is started, this class is instantiated before any of the application's components.
The subclass is optional; most applications won't need one. In the absence of a subclass, Android uses an instance of the base Application class.

CPPs are sent along just fine although they don't appear in the saving persisted state line.
=====================================
Invite mode types:
IN_SESSION
In Session mode denotes that the survey is presented at the point where the user accepts the invitation.
CONTACT
Contact mode denotes that the user can enter in an email address/mobile number to receive a link to the survey at a later time.
EXIT_SURVEY
Exit Survey mode denotes that the user receives an invite in session, then a survey link as a local notification after they close the app.
EXIT_INVITE
Exit Invite mode denotes that the user receives no invite during their app session - only a local notification after they close the app which takes them directly to the survey.
=====================================
Ran into an issue with EXIT_SURVEY, and I assume EXIT_INVITE, app crashes on close when trying to use these modes
update**** fixed this by changing name to an image in res/drawable "gnome". "gnome.jpg" & "gnome2" caused an error.
While using "gnome" works, I don't actually see the gnome picture on the notification, not sure why.

for dev portal documentation:
If you use exit_survey / exit invite, you must have an image resource with the default name ic_notification, or specify a different name for an image using the config file or else the application will crash

The notification icon cannot include the image extension (ie: .jpg)



 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void launchInvite(View v){
        ForeSee.showInviteForSurveyID("app_test_1");
    }
}
